function Pandoc (doc)
  doc.meta.references = pandoc.utils.references(doc)
  doc.meta.bibliography = nil
  for i, ref in ipairs(doc.meta.references or {}) do
    if not (ref.type:match 'webpage' or ref.type:match 'post-weblog') then
      ref.URL = nil
      ref.url = nil
      ref.DOI = nil
      ref.doi = nil
    end
  end
  return doc
end